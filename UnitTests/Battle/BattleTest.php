<?php
use Creature\AbstractCreature;
use Creature\CreatureFactory;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01.10.2017
 * Time: 14:00
 */
class BattleTest extends TestCase {

    public function creatureProvider() {

        $creature1 = [
            "name" => "test1",
            "stats" => [
                'health' => [
                    'min' => 60,
                    'max' => 90
                ],
                'strength' => [
                    'min' => 60,
                    'max' => 90
                ],
                'defence' => [
                    'min' => 40,
                    'max' => 60
                ],
                'speed' => [
                    'min' => 40,
                    'max' => 80
                ],
                'luck' => [
                    'min' => 25,
                    'max' => 40
                ]
            ]
        ];

        /**
         * @var AbstractCreature $creatureMock1;
         */
        $creatureMock1 = $this->getMockBuilder(AbstractCreature::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $creatureMock1->expects($this->any())->method("getName")->will($this->returnValue($creature1["name"]));
        $creatureMock1->expects($this->any())->method("getCreatureStatIntervals")->will($this->returnValue($creature1["stats"]));
        $creatureMock1->__construct();

        $creature2 = [
            "name" => "test2",
            "stats" => [
                'health' => [
                    'min' => 60,
                    'max' => 90
                ],
                'strength' => [
                    'min' => 60,
                    'max' => 90
                ],
                'defence' => [
                    'min' => 40,
                    'max' => 60
                ],
                'speed' => [
                    'min' => 40,
                    'max' => 80
                ],
                'luck' => [
                    'min' => 25,
                    'max' => 40
                ]
            ]
        ];

        /**
         * @var AbstractCreature $creatureMock2;
         */
        $creatureMock2 = $this->getMockBuilder(AbstractCreature::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $creatureMock2->expects($this->any())->method("getName")->will($this->returnValue($creature2["name"]));
        $creatureMock2->expects($this->any())->method("getCreatureStatIntervals")->will($this->returnValue($creature2["stats"]));
        $creatureMock2->__construct();

        return [
            [
                $creatureMock1, $creatureMock2
            ]
        ];

    }

    /**
     * @dataProvider creatureProvider
     * @param AbstractCreature $creature1
     * @param AbstractCreature $creature2
     */
    public function testConstructAndFirstStart(AbstractCreature $creature1, AbstractCreature $creature2) {

        $creatureClone1 = clone $creature1;
        $creatureClone2 = clone $creature2;

        $battle = new \Battle\Battle(2, $creature1, $creature2);

        if ($creature1->getSpeed() > $creature2->getSpeed()
            || (
                $creature1->getSpeed() === $creature2->getSpeed()
                && $creature1->getSpeed() > $creature2->getSpeed()
            )
        ) {

            $this->assertEquals($battle->getCurrentAttacker()->getName(), $creatureClone1->getName());
            $this->assertEquals($battle->getCurrentAttacker()->getCreatureStatIntervals(), $creatureClone1->getCreatureStatIntervals());

            $this->assertEquals($battle->getCurrentDefender()->getName(), $creatureClone2->getName());
            $this->assertEquals($battle->getCurrentDefender()->getCreatureStatIntervals(), $creatureClone2->getCreatureStatIntervals());

        } else {

            $this->assertEquals($battle->getCurrentAttacker()->getName(), $creatureClone2->getName());
            $this->assertEquals($battle->getCurrentAttacker()->getCreatureStatIntervals(), $creatureClone2->getCreatureStatIntervals());

            $this->assertEquals($battle->getCurrentDefender()->getName(), $creatureClone1->getName());
            $this->assertEquals($battle->getCurrentDefender()->getCreatureStatIntervals(), $creatureClone1->getCreatureStatIntervals());

        }

        $this->assertEquals($battle->getCurrentRound(), 1);
        $this->assertEquals($battle->getWinner(), null);

    }

    /**
     * @dataProvider creatureProvider
     * @param AbstractCreature $creature1
     * @param AbstractCreature $creature2
     */
    public function executeTurn(AbstractCreature $creature1, AbstractCreature $creature2) {

        $battle = new \Battle\Battle(2, $creature1, $creature2);

        $initialAttacker = clone $battle->getCurrentAttacker();
        $initialDefender = clone $battle->getCurrentDefender();

        $battle->executeTurn();

        $this->assertEquals($battle->getCurrentAttacker()->getName(), $initialDefender->getName());
        $this->assertEquals($battle->getCurrentAttacker()->getCreatureStatIntervals(), $initialDefender->getCreatureStatIntervals());

        $this->assertEquals($battle->getCurrentDefender()->getName(), $initialAttacker->getName());
        $this->assertEquals($battle->getCurrentDefender()->getCreatureStatIntervals(), $initialAttacker->getCreatureStatIntervals());

        $this->assertEquals($battle->getCurrentRound(), 2);

    }

}