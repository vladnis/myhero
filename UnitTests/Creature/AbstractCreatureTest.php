<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 01.10.2017
 * Time: 12:50
 */

use Creature\AbstractCreature;
use PHPUnit\Framework\TestCase;

class AbstractCreatureTest extends TestCase {

    /**
     * Data provider for TestConstruct;
     * @return array
     */
    public function providerForTestConstruct() {
        return [
            [
                "test",
                [
                    'health' => [
                        'min' => 60,
                        'max' => 90
                    ],
                    'strength' => [
                        'min' => 60,
                        'max' => 90
                    ],
                    'defence' => [
                        'min' => 40,
                        'max' => 60
                    ],
                    'speed' => [
                        'min' => 40,
                        'max' => 60
                    ],
                    'luck' => [
                        'min' => 25,
                        'max' => 40
                    ]
                ],
                false
            ],
            [
                "test",
                [
                    'health' => [
                        'min' => 0,
                        'max' => 0
                    ],
                    'strength' => [
                        'min' => 60,
                        'max' => 90
                    ],
                    'defence' => [
                        'min' => 40,
                        'max' => 60
                    ],
                    'speed' => [
                        'min' => 40,
                        'max' => 60
                    ],
                    'luck' => [
                        'min' => 25,
                        'max' => 40
                    ]
                ],
                true
            ]
        ];
    }

    /**
     * Data provider for TestConstruct;
     * @return array
     */
    public function providerForTestAttackAndDefence() {
        return [
            [
                [
                    "name" => "test1",
                    "stats" => [
                        'health' => [
                            'min' => 60,
                            'max' => 90
                        ],
                        'strength' => [
                            'min' => 60,
                            'max' => 90
                        ],
                        'defence' => [
                            'min' => 40,
                            'max' => 60
                        ],
                        'speed' => [
                            'min' => 40,
                            'max' => 60
                        ],
                        'luck' => [
                            'min' => 25,
                            'max' => 40
                        ]
                    ]
                ],
                [
                    "name" => "test2",
                    "stats" => [
                        'health' => [
                            'min' => 60,
                            'max' => 90
                        ],
                        'strength' => [
                            'min' => 60,
                            'max' => 90
                        ],
                        'defence' => [
                            'min' => 40,
                            'max' => 60
                        ],
                        'speed' => [
                            'min' => 40,
                            'max' => 60
                        ],
                        'luck' => [
                            'min' => 25,
                            'max' => 40
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Test constructor.
     *
     * @dataProvider providerForTestConstruct
     * @param $getNameReturnValue
     * @param $getCreatureIntervalReturnValue
     */
    public function testConstruct($getNameReturnValue, $getCreatureIntervalReturnValue, $isDead) {

        /**
         * @var AbstractCreature $stub;
         */
        $stub = $this->getMockBuilder(AbstractCreature::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $stub->expects($this->any())->method("getName")->will($this->returnValue($getNameReturnValue));
        $stub->expects($this->any())->method("getCreatureStatIntervals")->will($this->returnValue($getCreatureIntervalReturnValue));

        $stub->__construct();

        foreach ($getCreatureIntervalReturnValue as $key => $value) {
            $functionName = "get" . ucfirst($key);
            $this->assertTrue(
                $value["min"] <= $stub->$functionName()
                && $stub->$functionName() <= $value["max"]
            );
        }

        $this->assertTrue($stub->isDead() === $isDead);
        $this->assertTrue($stub->getCreatureStatIntervals() === $getCreatureIntervalReturnValue);

    }

    /**
     * @dataProvider providerForTestAttackAndDefence
     * @param $creature1
     * @param $creature2
     */
    public function testAttackAndDefence($creature1, $creature2) {

        $creature2['stats']['luck'] = [
            'max' => 0,
            'min' => 0
        ];

        /**
         * @var AbstractCreature $creatureMock1;
         */
        $creatureMock1 = $this->getMockBuilder(AbstractCreature::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $creatureMock1->expects($this->any())->method("getName")->will($this->returnValue($creature1["name"]));
        $creatureMock1->expects($this->any())->method("getCreatureStatIntervals")->will($this->returnValue($creature1["stats"]));
        $creatureMock1->__construct();


        /**
         * @var AbstractCreature $creatureMock2;
         */
        $creatureMock2 = $this->getMockBuilder(AbstractCreature::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $creatureMock2->expects($this->any())->method("getName")->will($this->returnValue($creature2["name"]));
        $creatureMock2->expects($this->any())->method("getCreatureStatIntervals")->will($this->returnValue($creature2["stats"]));
        $creatureMock2->__construct();

        $currentHealth = $creatureMock2->getHealth();
        $damage = ($creatureMock1->getStrength() - $creatureMock2->getDefence());
        $creatureMock1->attack($creatureMock2);

        $this->assertTrue($creatureMock2->getHealth() === $currentHealth - $damage);


    }

}
