<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 23:35
 */

use PHPUnit\Framework\TestCase;

class CreatureFactoryTest extends TestCase {

    /**
     * Test Hero generation.
     */
    public function testGenerateHero() {
        $creature = \Creature\CreatureFactory::generateCreature(\Creature\CreatureFactory::CREATURE_HERO);
        $this->assertTrue($creature instanceof \Creature\Creatures\Hero);
    }

    /**
     * Test Beast generation.
     */
    public function testGenerateBest() {
        $creature = \Creature\CreatureFactory::generateCreature(\Creature\CreatureFactory::CREATURE_BEAST);
        $this->assertTrue($creature instanceof \Creature\Creatures\Beast);
    }

    /**
     * @expectedException \Exceptions\InvalidParameterException
     */
    public function testInvalidArgument() {
        \Creature\CreatureFactory::generateCreature(4);
    }

}
