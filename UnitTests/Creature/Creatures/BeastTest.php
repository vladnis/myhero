<?php
use Creature\Creatures\Beast;
use Creature\Creatures\Hero;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 23:45
 */
class BeastTest extends TestCase {

    /**
     * @var Hero
     */
    private $instance;

    public function setUp() {
        $this->instance = new Beast();
    }

    public function testStats() {
        $this->assertEquals(
            $this->instance->getCreatureStatIntervals(),
            [
                'health' => [
                    'min' => 60,
                    'max' => 90
                ],
                'strength' => [
                    'min' => 60,
                    'max' => 90
                ],
                'defence' => [
                    'min' => 40,
                    'max' => 60
                ],
                'speed' => [
                    'min' => 40,
                    'max' => 60
                ],
                'luck' => [
                    'min' => 25,
                    'max' => 40
                ]
            ]
        );
    }

    public function testName() {
        $this->assertEquals("Beast", $this->instance->getName());
    }

}