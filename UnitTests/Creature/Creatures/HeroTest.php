<?php
use Creature\Creatures\Hero;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 23:45
 */
class HeroTest extends TestCase {

    /**
     * @var Hero
     */
    private $instance;

    public function setUp() {
        $this->instance = new Hero();
    }

    public function testStats() {
        $this->assertEquals(
            $this->instance->getCreatureStatIntervals(),
            [
                'health' => [
                    'min' => 70,
                    'max' => 100
                ],
                'strength' => [
                    'min' => 70,
                    'max' => 80
                ],
                'defence' => [
                    'min' => 45,
                    'max' => 55
                ],
                'speed' => [
                    'min' => 40,
                    'max' => 50
                ],
                'luck' => [
                    'min' => 10,
                    'max' => 30
                ]
            ]
        );
    }

    public function testName() {
        $this->assertEquals("Hero", $this->instance->getName());
    }


}
