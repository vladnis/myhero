<?php
namespace AutoLoader;

use Logger\Logger;

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 21:47
 */
class AutoLoader {

    /**
     * Register all auto load functions.
     */
    public static function initialise() {
        spl_autoload_register("AutoLoader\\AutoLoader::loadClasses");
    }

    /*
     * Auto load classes for File Access.
     * @throws EndpointException
     */
    public static function loadClasses($className) {
        $parts = explode('\\', $className);
        $className = end($parts);
        unset($parts[sizeof($parts) - 1]);
        $folderPath = implode("/", $parts);

        $filePathPlugins = __DIR__ . '/../' . $folderPath . "/" . $className . ".php";

        if (file_exists($filePathPlugins)) {
            require_once($filePathPlugins);
            return;
        }

        echo "AutoLoader: File $filePathPlugins not found.", __FILE__, __LINE__;

    }

}