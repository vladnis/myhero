<?php

namespace Battle;

use BattleFinishedException;
use Creature\AbstractCreature;
use Logger\Logger;

class Battle {

    /**
     * @var int
     */
    private $currentRound = 1;

    /**
     * @var int.
     */
    private $rounds;

    /**
     * @var AbstractCreature
     */
    private $currentAttacker;

    /**
     * @var AbstractCreature
     */
    private $currentDefender;

    /**
     * @var AbstractCreature
     */
    private $winner;

    /**
     * Battle constructor.
     *
     * @param $rounds
     * @param AbstractCreature $player1
     * @param AbstractCreature $player2
     */
    function __construct(int $rounds, AbstractCreature $player1, AbstractCreature $player2) {

        $this->rounds = $rounds;
        $this->chooseWhoStarts($player1, $player2);

    }

    /**
     * Choose the first player to attack.
     *
     * @param AbstractCreature $player1
     * @param AbstractCreature $player2
     */
    private function chooseWhoStarts(AbstractCreature $player1, AbstractCreature $player2) {

        if ($player1->getSpeed() > $player2->getSpeed()
            || (
                $player1->getSpeed() === $player2->getSpeed()
                && $player1->getLuck() > $player2->getLuck()
            )
        ) {
            $this->currentAttacker = $player1;
            $this->currentDefender = $player2;
            return;
        }
        $this->currentAttacker = $player2;
        $this->currentDefender = $player1;

    }

    /**
     * Get current attacker.
     *
     * @return AbstractCreature
     */
    public function getCurrentAttacker() {
        return $this->currentAttacker;
    }

    /**
     * Get current defendant.
     *
     * @return AbstractCreature
     */
    public function getCurrentDefender(): AbstractCreature {
        return $this->currentDefender;
    }

    /**
     * Execute one turn.
     * @throws BattleFinishedException
     */
    public function executeTurn() {

        if ($this->isBattleFinished()) {
            throw new BattleFinishedException();
        }

        Logger::debug("Starting round: " . $this->currentRound);

        $this->currentAttacker->attack($this->currentDefender);

        $this->currentRound++;

        if ($this->currentDefender->isDead()) {

            $this->winner = $this->currentAttacker;
            Logger::debug($this->currentDefender->getName() . " is dead");
            Logger::debug("");

            return;

        }

        Logger::debug($this->currentDefender->getName() . " remaining health: " . $this->currentDefender->getHealth());

        $aux  = $this->currentAttacker;
        $this->currentAttacker = $this->currentDefender;
        $this->currentDefender = $aux;


        Logger::debug("");

    }

    /**
     * Get current round.
     *
     * @return int
     */
    public function getCurrentRound(): int {
        return $this->currentRound;
    }

    /**
     * Test if battle is finished.
     *
     * @return bool
     */
    public function isBattleFinished(): bool {
        return $this->currentRound > $this->rounds || isset($this->winner);
    }

    /**
     * Get winner of the battle.
     *
     * @return AbstractCreature|null
     */
    public function getWinner() {
        return $this->winner;
    }

    /**
     * Check if battle has winner.
     *
     * @return bool
     */
    public function hasWinner() {
        return isset($this->winner);
    }

}
