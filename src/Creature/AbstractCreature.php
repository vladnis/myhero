<?php

namespace Creature;

use Creature\Exceptions\InvalidCreatureStatsException;
use Logger\Logger;
use Skill\AbstractSkill;
use Skill\SkillFactory;

abstract class AbstractCreature {

    /**
     * @var int
     */
    private $health;

    /**
     * @var int
     */
    private $strength;

    /**
     * @var int
     */
    private $defence;

    /**
     * @var int
     */
    private $speed;

    /**
     * @var int
     */
    private $luck;

    /**
     * @var AbstractSkill[]
     */
    private $skills = [];

    /**
     * Get creature name.
     *
     * @return string
     */
    abstract function getName(): string;

    /**
     * Get creature characteristics.
     *
     * @return array
     */
    abstract function getCreatureStatIntervals(): array;

    /**
     * AbstractCreature constructor.
     */
    function __construct() {
        $this->generateStats();
    }

    /**
     * Generate new stats.
     */
    private function generateStats() {

        $statsInterval = $this->getCreatureStatIntervals();

        $requiredKeys = ['health', 'strength', 'defence', 'speed', 'luck'];

        foreach ($requiredKeys as $key) {
            if (!isset($statsInterval[$key])
                || !isset($statsInterval[$key]['min'])
                || !isset($statsInterval[$key]['max'])
            ) {
                throw new InvalidCreatureStatsException();
            }

            $this->$key = rand($statsInterval[$key]['min'], $statsInterval[$key]['max']);
        }

    }

    /**
     * Attack another creature.
     *
     * @param AbstractCreature $attackedCreature
     * @throws InvalidCreatureStatsException
     */
    public function attack(AbstractCreature $attackedCreature) {

        Logger::debug($this->getName() . " attacks " . $attackedCreature->getName());

        // Can we use a skill?
        foreach ($this->skills as $skill) {
            if (!$skill->activateSkill() || $skill->isDefenceSkill()) {
                continue;
            }
            $skill->attack($attackedCreature);
            return;
        }

        $attackedCreature->defend($this);

    }

    /**
     * Let creature defend its self.
     *
     * @param AbstractCreature $attacker
     * @throws InvalidCreatureStatsException
     */
    public function defend(AbstractCreature $attacker) {

        $damage = $attacker->getStrength() - $this->getDefence();

        Logger::debug($this->getName() . " receives attack with initial damage: " . $damage);

        // Can we use a skill?
        foreach ($this->skills as $skill) {
            if (!$skill->activateSkill() || $skill->isAttackSkill()) {
                continue;
            }
            $damage = $skill->defend($attacker);
            break;
        }

        if ($this->isLucky()) {

            Logger::debug($this->getName() . " got lucky. Attacker missed.");
            return;
        }
        $this->addDamageToHealth($damage);

    }

    /**
     * Get current speed of the creature.
     * @return int
     * @throws InvalidCreatureStatsException
     */
    public function getSpeed(): int {
        if (!isset($this->speed)) {
            throw new InvalidCreatureStatsException();
        }
        return $this->speed;
    }

    /**
     * Get current luck of the creature.
     * @return int
     * @throws InvalidCreatureStatsException
     */
    public function getLuck(): int {
        if (!isset($this->luck)) {
            throw new InvalidCreatureStatsException();
        }
        return $this->luck;
    }

    /**
     * Add new skill.
     *
     * @param int $skill
     */
    public function addSkill(int $skill) {
        $this->skills[] = SkillFactory::generateSkill($skill, $this);
    }

    /**
     * Is creature dead.
     * @return bool
     * @throws InvalidCreatureStatsException
     */
    public function isDead(): bool {
        return $this->getHealth() <= 0;
    }

    /**
     * Get current health of the creature.
     * @return int
     * @throws InvalidCreatureStatsException
     */
    public function getHealth(): int {
        if (!isset($this->health)) {
            throw new InvalidCreatureStatsException();
        }
        return $this->health;
    }

    /**
     * Is creature lucky this time.
     * @return bool
     * @throws InvalidCreatureStatsException
     */
    public function isLucky(): bool {
        $luck = $this->getLuck();

        if (rand(1, 100) < $luck) {
            return true;
        }
        return false;
    }

    /**
     * Get strength.
     * @return int
     * @throws InvalidCreatureStatsException
     */
    public function getStrength(): int {
        if (!isset($this->strength)) {
            throw new InvalidCreatureStatsException();
        }
        return $this->strength;
    }

    /**
     * Get defence.
     * @return int.
     * @throws InvalidCreatureStatsException
     */
    public function getDefence(): int {
        if (!isset($this->defence)) {
            throw new InvalidCreatureStatsException();
        }
        return $this->defence;
    }

    /**
     * Add damage to creature.
     *
     * @param int $damage
     */
    private function addDamageToHealth(int $damage) {
        $this->health -= $damage;

        Logger::debug($this->getName() . " taken $damage damage");

    }

}
