<?php

namespace Creature;

use Creature\Creatures\Beast;
use Creature\Creatures\Hero;
use Exceptions\InvalidParameterException;

class CreatureFactory {

    const CREATURE_BEAST = 1;
    const CREATURE_HERO = 2;

    /**
     * Generate new skill.
     *
     * @param int|string $creature
     * @return AbstractCreature
     * @throws InvalidParameterException
     */
    public static function generateCreature(int $creature): AbstractCreature {

        switch ($creature) {
            case self::CREATURE_BEAST:
                return new Beast();
                break;
            case self::CREATURE_HERO:
                return new Hero();
                break;
            default:
                throw new InvalidParameterException('creature');
        }

    }

}
