<?php

namespace Creature\Creatures;

use Creature\AbstractCreature;

class Beast extends AbstractCreature {

    /**
     * Get creature name.
     *
     * @return string
     */
    function getName(): string {
        return "Beast";
    }

    /**
     * Get creature characteristics.
     *
     * @return array
     */
    function getCreatureStatIntervals(): array {

        return [
            'health' => [
                'min' => 60,
                'max' => 90
            ],
            'strength' => [
                'min' => 60,
                'max' => 90
            ],
            'defence' => [
                'min' => 40,
                'max' => 60
            ],
            'speed' => [
                'min' => 40,
                'max' => 60
            ],
            'luck' => [
                'min' => 25,
                'max' => 40
            ]
        ];

    }
}
