<?php

namespace Creature\Creatures;

use Creature\AbstractCreature;
use Skill\SkillFactory;

class Hero extends  AbstractCreature {

    /**
     * Hero constructor.
     */
    public function __construct() {
        parent::__construct();

        $this->addSkill(SkillFactory::SKILL_RAPID_FIRE);
        $this->addSkill(SkillFactory::SKILL_MAGIC_SHIELD);

    }

    /**
     * Get creature name.
     *
     * @return string
     */
    function getName(): string {
        return "Hero";
    }


    /**
     * Get creature characteristics.
     *
     * @return array
     */
    function getCreatureStatIntervals(): array {
        return [
            'health' => [
                'min' => 70,
                'max' => 100
            ],
            'strength' => [
                'min' => 70,
                'max' => 80
            ],
            'defence' => [
                'min' => 45,
                'max' => 55
            ],
            'speed' => [
                'min' => 40,
                'max' => 50
            ],
            'luck' => [
                'min' => 10,
                'max' => 30
            ]
        ];
    }
}
