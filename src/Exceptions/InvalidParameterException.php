<?php

namespace Exceptions;

use Throwable;

class InvalidParameterException extends \Exception {

    public function __construct($parameterName, $code = 0, Throwable $previous = null) {
        parent::__construct($parameterName . " is invalid", $code, $previous);
    }

}