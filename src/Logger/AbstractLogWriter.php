<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 15:37
 */

namespace Logger;


use Logger\LogWriters\ConsoleLogWriter;

abstract class AbstractLogWriter {

    /**
     * Write message.
     *
     * @param $message
     * @return bool
     */
    abstract function writeLogMessage($message);

    /**
     * Get log writer.
     *
     * @return ConsoleLogWriter
     */
    public static function generateInstance() {
        return new ConsoleLogWriter();
    }

}