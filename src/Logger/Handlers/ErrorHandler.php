<?php

namespace Logger\Handlers;


use Logger\Logger;

class ErrorHandler {

    /**
     * Initialise error handler.
     */
    public static function initialise() {
        set_error_handler('Logger\Handlers\ErrorHandler::customHandler');
    }

    /**
     * Custom error handler function.
     *
     * @param int $errNo
     * @param string $errMessage
     * @param string $errFile
     * @param int $errLine
     * @return bool
     */
    public static function customHandler($errNo, $errMessage, $errFile, $errLine) {
        Logger::error($errMessage, $errFile,$errLine);
        exit(1);
    }

}
