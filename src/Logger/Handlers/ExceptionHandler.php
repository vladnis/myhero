<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 17:49
 */

namespace Logger\Handlers;


use Logger\Logger;

class ExceptionHandler {

    /**
     * Initialise error handler.
     */
    public static function initialise() {
        set_exception_handler('Logger\Handlers\ExceptionHandler::customHandler');
    }

    /**
     * Custom exception handler.
     *
     * @param \Exception $exception
     */
    public static function customHandler($exception) {
        Logger::error($exception->getTraceAsString() . "\n" . $exception->getMessage(), $exception->getFile(), $exception->getLine());
        exit(1);
    }

}
