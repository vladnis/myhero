<?php

namespace Logger\LogWriters;

use Logger\AbstractLogWriter;

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 15:40
 */
class ConsoleLogWriter extends AbstractLogWriter {

    /**
     * Write message.
     *
     * @param $message
     * @return bool
     */
    function writeLogMessage($message) {
        echo $message;

        return true;
    }

}