<?php

namespace Logger;

use Exceptions\InvalidParameterException;
use Patterns\Singleton;

class Logger {

    use Singleton;

    /**
     * @var int
     */
    private $logLevel;

    /**
     * @var AbstractLogWriter
     */
    private $logWriter;

    /**
     * Log levels.
     */
    const LOG_LEVEL_DEBUG = 1;
    const LOG_LEVEL_WARNING = 2;
    const LOG_LEVEL_ERROR = 3;

    /**
     * Logger constructor.
     *
     * @param int $logLevel Log messages that are above set log level.
     * @param AbstractLogWriter $logWriter
     */
    private function __construct(int $logLevel, AbstractLogWriter $logWriter) {

        $this->logWriter = $logWriter;
        $this->logLevel = $logLevel;

    }

    /**
     * Log message with the set log level.
     *
     * @param int $logLevel
     * @param string|mixed $message
     * @return bool
     *
     * @throws InvalidParameterException
     */
    public function log(int $logLevel, ...$messages) {

        if ($logLevel < $this->logLevel) {
            return false;
        }

        switch ($logLevel) {
            case self::LOG_LEVEL_DEBUG:
                $debugLevelText = 'debug';
                break;
            case self::LOG_LEVEL_ERROR:
                $debugLevelText = 'error';
                break;
            case self::LOG_LEVEL_WARNING:
                $debugLevelText = 'warning';
                break;
            default:
                throw new InvalidParameterException('logLevel');
        }

        $this->logWriter->writeLogMessage(date('c'). ' - ' . $debugLevelText  . ': ');

        foreach ($messages as $message) {
            $this->logWriter->writeLogMessage($message . PHP_EOL);
        }

        return true;

    }

    /**
     * Log debug message.
     *
     * @param array $args
     * @return bool
     */
    public static function debug(...$args) {
        return Logger::getInstance()->log(Logger::LOG_LEVEL_DEBUG, ...$args);
    }


    /**
     * Log debug message.
     *
     * @param array $args
     * @return bool
     */
    public static function error(...$args) {
        return Logger::getInstance()->log(Logger::LOG_LEVEL_ERROR, ...$args);
    }

    /**
     * Log debug message.
     *
     * @param array $args
     * @return bool
     */
    public static function warning(...$args) {
        return Logger::getInstance()->log(Logger::LOG_LEVEL_WARNING, ...$args);
    }

}
