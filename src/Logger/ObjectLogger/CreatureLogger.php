<?php

namespace Logger\ObjectLogger;
use Creature\AbstractCreature;
use Logger\Logger;

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 23:14
 */
class CreatureLogger {

    /**
     * Log creature information.
     *
     * @param AbstractCreature $creature
     */
    public static function logCreatureInfo(AbstractCreature $creature) {

        Logger::debug("Creature name: " . $creature->getName());
        Logger::debug("Creature health: " . $creature->getHealth());
        Logger::debug("Creature strength: " . $creature->getStrength());
        Logger::debug("Creature defence: " . $creature->getDefence());
        Logger::debug("Creature speed: " . $creature->getSpeed());
        Logger::debug("Creature luck: " . $creature->getLuck());

    }

}
