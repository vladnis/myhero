<?php

namespace Patterns;

/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 15:17
 */
trait Singleton {

    /**
     * @var array
     */
    private static $instances = [];

    /**
     * Get instance of class.
     * @param array $args
     * @return static
     */
    public static function getInstance(...$args) {
        $className = get_called_class();

        if (!isset(self::$instances[$className])) {
            self::$instances[$className] = new $className(...$args);
        }

        return self::$instances[$className];
    }

    /**
     * Singleton constructor.
     */
    private function __construct() {}

}