<?php

namespace Skill;

use Creature\AbstractCreature;
use Skill\Exceptions\CannotUseSkill;

abstract class AbstractSkill {

    const ATTACK_SKILL = 1;
    const DEFENCE_SKILL = 2;

    /**
     * @var AbstractCreature
     */
    private $creature;

    /**
     * @var bool
     */
    private $canUseSkill = false;

    /**
     * AbstractSkill constructor.
     * @param AbstractCreature $creature
     */
    function __construct(AbstractCreature $creature) {
        $this->creature = $creature;
    }

    /**
     * Get the change there is to use this skill.
     *
     * @return int
     */
    abstract function getChanceOfUsage(): int;

    /**
     * Attack using the skill.
     * @param AbstractCreature $defender
     */
    public function attack(AbstractCreature $defender) {
        $this->checkIfSkillWasActivated();
        $this->_attack($defender);
        $this->deactivateSkill();
    }

    /**
     * Attack using the skill.
     * @param AbstractCreature $defender
     */
    abstract public function _attack(AbstractCreature $defender);

    /**
     * Defend using the skill.
     * @param AbstractCreature $attacker
     * @return int
     */
    public function defend(AbstractCreature $attacker) {
        $this->checkIfSkillWasActivated();
        $this->deactivateSkill();

        return $this->_defend($attacker);
    }

    /**
     * Defend using the skill.
     * @param AbstractCreature $attacker
     * @return int
     */
    abstract public function _defend(AbstractCreature $attacker): int;

    /**
     * Get skill type.
     * @return int
     */
    abstract public function getSkillType(): int;

    /**
     * Use skill?
     * @return bool
     */
    public function activateSkill(): bool {

        if (rand(1, 100) < $this->getChanceOfUsage()) {
            $this->canUseSkill = true;
            return true;
        }
        return false;
    }

    /**
     * Deactivate skill.
     */
    public function deactivateSkill() {
        $this->canUseSkill = false;
    }

    /**
     * Check if skill can be used
     * @throws CannotUseSkill
     */
    private function checkIfSkillWasActivated() {
        if (!$this->canUseSkill) {
            throw new CannotUseSkill();
        }
    }

    /**
     * Get creature associated with skill.
     * @return AbstractCreature
     */
    public function getCreature(): AbstractCreature {
        return $this->creature;
    }

    /**
     * Is attack skill?
     * @return bool
     */
    public function isAttackSkill(): bool {
        return $this->getSkillType() === self::ATTACK_SKILL;
    }

    /**
     * Is defence skill?
     * @return bool
     */
    public function isDefenceSkill(): bool {
        return $this->getSkillType() === self::DEFENCE_SKILL;
    }

}
