<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 19:21
 */

namespace Skill;


use Creature\AbstractCreature;
use Exceptions\InvalidParameterException;
use Skill\Skills\MagicShield;
use Skill\Skills\RapidStrike;

class SkillFactory {

    const SKILL_MAGIC_SHIELD = 1;
    const SKILL_RAPID_FIRE = 2;

    /**
     * Generate new skill.
     *
     * @param int $skill
     * @param AbstractCreature $creature
     * @return AbstractSkill
     * @throws InvalidParameterException
     */
    public static function generateSkill(int $skill, AbstractCreature $creature): AbstractSkill {

        switch ($skill) {
            case self::SKILL_MAGIC_SHIELD:
                return new MagicShield($creature);
                break;
            case self::SKILL_RAPID_FIRE:
                return new RapidStrike($creature);
                break;
            default:
                throw new InvalidParameterException('skill');
        }

    }

}
