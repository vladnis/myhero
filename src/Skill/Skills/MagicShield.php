<?php

namespace Skill\Skills;

use Creature\AbstractCreature;
use Logger\Logger;
use Skill\AbstractSkill;
use Skill\Exceptions\CannotUseSkill;

class MagicShield extends AbstractSkill {

    /**
     * Get the change there is to use this skill.
     *
     * @return int
     */
    function getChanceOfUsage(): int {
        return 10;
    }

    /**
     * Attack using the skill.
     * @param AbstractCreature $defender
     * @throws CannotUseSkill
     */
    public function _attack(AbstractCreature $defender) {
        throw new CannotUseSkill();
    }

    /**
     * Defend using the skill.
     * @param AbstractCreature $attacker
     * @return int
     */
    public function _defend(AbstractCreature $attacker): int {
        Logger::error($this->getCreature()->getName() . ' uses Magic Shield (damage is half)');

        return ($attacker->getStrength() - $this->getCreature()->getDefence()) / 2;
    }

    /**
     * Get skill type.
     * @return int
     */
    public function getSkillType(): int {
        return AbstractSkill::DEFENCE_SKILL;
    }

}
