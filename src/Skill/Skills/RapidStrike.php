<?php

namespace Skill\Skills;

use Creature\AbstractCreature;
use Logger\Logger;
use Skill\AbstractSkill;
use Skill\Exceptions\CannotUseSkill;

class RapidStrike extends AbstractSkill {

    /**
     * Get the change there is to use this skill.
     *
     * @return int
     */
    function getChanceOfUsage(): int {
        return 20;
    }

    /**
     * Attack using the skill.
     * @param AbstractCreature $defender
     */
    public function _attack(AbstractCreature $defender) {

        Logger::error($this->getCreature()->getName() . ' uses Rapid Strike (double attack)');

        $defender->defend($this->getCreature());
        $defender->defend($this->getCreature());

    }

    /**
     * Defend using the skill.
     * @param AbstractCreature $attacker
     * @return int
     * @throws CannotUseSkill
     */
    public function _defend(AbstractCreature $attacker): int {
        throw new CannotUseSkill();
    }

    /**
     * Get skill type.
     * @return int
     */
    public function getSkillType(): int {
        return AbstractSkill::ATTACK_SKILL;
    }
}
