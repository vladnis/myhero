<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 30.09.2017
 * Time: 21:51
 */

use AutoLoader\AutoLoader;
use Logger\AbstractLogWriter;
use Logger\Handlers\ErrorHandler;
use Logger\Handlers\ExceptionHandler;
use Logger\Logger;

ini_set("html_errors" , false);
ini_set("error_reporting" , E_ALL);
ini_set("display_errors" , E_ALL);

require_once 'AutoLoader/AutoLoader.php';

/**
 * Initialise AutoLoader
 */
AutoLoader::initialise();

/**
 * Initialise logger.
 */
$logger = Logger::getInstance(Logger::LOG_LEVEL_DEBUG, AbstractLogWriter::generateInstance());

/**
 * Initialise error handling.
 */
ErrorHandler::initialise();
ExceptionHandler::initialise();
