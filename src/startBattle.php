<?php

use Battle\Battle;
use Creature\CreatureFactory;
use Logger\Logger;
use Logger\ObjectLogger\CreatureLogger;

include_once 'bootstrap.php';

/**
 * Initialise players
 */
$hero = CreatureFactory::generateCreature(CreatureFactory::CREATURE_HERO);
$beast = CreatureFactory::generateCreature(CreatureFactory::CREATURE_BEAST);

Logger::debug("##### Creatures information ####");
CreatureLogger::logCreatureInfo($hero);
Logger::debug("#####  ============ ####" . PHP_EOL);
CreatureLogger::logCreatureInfo($beast);
Logger::debug("#####  ============ ####" . PHP_EOL);

/**
 * Initialise battle.
 */
$battle = new Battle(20, $hero, $beast);

do {

    $battle->executeTurn();

} while(!$battle->isBattleFinished());

if ($battle->hasWinner()) {
    Logger::debug("Winner is: " . $battle->getWinner()->getName());
} else {
    Logger::debug("There is no winner");
}
